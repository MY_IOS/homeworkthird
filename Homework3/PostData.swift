//
//  PostData.swift
//  Homework3
//
//  Created by Apple on 4/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class PostData {
    var userPostProfileImage : String?
    var userPostName : String?
    var postDate : String?
    var postLocation : String?
    var postStatus : String?
    var postText : String?
    var postImage : String?
    var likePostNumber : String?
    var commentPostNumber : String?
    var sharePostNumber : String?
    
    init(userPostProfileImage : String,
         userPostName : String,
         postDate : String,
         postLocation : String,
         postStatus :String,
         postText : String,
         postImage: String,
         likePostNumber : String,
         commentPostNumber: String,
         sharePostNumber :String)
    {
        self.userPostProfileImage = userPostProfileImage
        self.userPostName = userPostName
        self.postDate = postDate
        self.postLocation = postLocation
        self.postStatus = postStatus
        self.postText = postText
        self.postImage = postImage
        self.likePostNumber = likePostNumber
        self.commentPostNumber = commentPostNumber
        self.sharePostNumber = sharePostNumber
        
    }
    
    
}
