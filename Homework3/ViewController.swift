//
//  ViewController.swift
//  Homework3
//
//  Created by Apple on 3/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit




class ViewController: UIViewController {
    
    @IBOutlet weak var viewCell: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let postText = UINib(nibName: "TextPost", bundle: nil)
        viewCell.register(postText, forCellReuseIdentifier: "textPost")
        let postIamge = UINib(nibName: "TextImage", bundle: nil)
        viewCell.register(postIamge, forCellReuseIdentifier: "textImage")
        
    }


}



extension ViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (postData.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let postText = tableView.dequeueReusableCell(withIdentifier: "textPost") as! TextPost
        let postImage = tableView.dequeueReusableCell(withIdentifier: "textImage") as! TextImage
        
        if postData[indexPath.row].postImage == ""{
            postText.setPostInfo(postData: postData[indexPath.row])
            return postText
        }
        else{
            postImage.setPostInfo(postData: postData[indexPath.row])
            return postImage
        }
        
    }
    
    
}

var postData: [PostData] = [
    PostData(userPostProfileImage : "profile",
             userPostName: "Sida Hoil",
             postDate: "Just now",
             postLocation: "HRD",
             postStatus: "🌍",
             postText: "This is a third homework. It uses constraint to fixed each ui view in this app.This is a third homework. It uses constraint to fixed each ui view in this app.",
             postImage: "",
             likePostNumber: "1.5K",
             commentPostNumber: "100 Comments",
             sharePostNumber: "20 Shares"),
    PostData(userPostProfileImage : "Waterfall",
             userPostName: "Flower Lover",
             postDate: "2 hrs",
             postLocation: "Phnom Penh",
             postStatus: "🌍",
             postText: "This is my love flower",
             postImage: "Landscape",
             likePostNumber: "250",
             commentPostNumber: "35 Comments",
             sharePostNumber: "10 Shares"),
    PostData(userPostProfileImage : "profile",
             userPostName: "Sida Hoil",
             postDate: "10 hrs",
             postLocation: "Phnom Penh",
             postStatus: "🌍",
             postText: "Want to go this beautifull place",
             postImage: "Waterfall",
             likePostNumber: "300",
             commentPostNumber: "50 Comments",
             sharePostNumber: "20 Share"),
    PostData(userPostProfileImage : "profile",
             userPostName: "Sida Hoil",
             postDate: "Just now",
             postLocation: "HRD",
             postStatus: "🌍",
             postText: "This is a third homework. It uses constraint to fixed each ui view in this app.This is a third homework. It uses constraint to fixed each ui view in this app.",
             postImage: "",
             likePostNumber: "1.5K",
             commentPostNumber: "100 Comments",
             sharePostNumber: "20 Shares"),
    PostData(userPostProfileImage : "profile",
             userPostName: "Flower Lover",
             postDate: "2 hrs",
             postLocation: "Phnom Penh",
             postStatus: "🌍",
             postText: "",
             postImage: "Waterfall",
             likePostNumber: "250",
             commentPostNumber: "35 Comments",
             sharePostNumber: "10 Shares"),
    PostData(userPostProfileImage : "profile",
             userPostName: "Sida Hoil",
             postDate: "10 hrs",
             postLocation: "Phnom Penh",
             postStatus: "🌍",
             postText: "Testing",
             postImage: "",
             likePostNumber: "300",
             commentPostNumber: "50 Comments",
             sharePostNumber: "20 Share"),
    PostData(userPostProfileImage : "profile",
             userPostName: "Sida Hoil",
             postDate: "Just now",
             postLocation: "HRD",
             postStatus: "🌍",
             postText: "This is a third homework. It uses constraint to fixed each ui view in this app.This is a third homework. It uses constraint to fixed each ui view in this app.",
             postImage: "",
             likePostNumber: "1.5K",
             commentPostNumber: "100 Comments",
             sharePostNumber: "20 Shares"),
    PostData(userPostProfileImage : "Waterfall",
             userPostName: "Flower Lover",
             postDate: "2 hrs",
             postLocation: "Phnom Penh",
             postStatus: "🌍",
             postText: "",
             postImage: "Landscape",
             likePostNumber: "250",
             commentPostNumber: "35 Comments",
             sharePostNumber: "10 Shares"),
    PostData(userPostProfileImage : "profile",
             userPostName: "Sida Hoil",
             postDate: "10 hrs",
             postLocation: "Phnom Penh",
             postStatus: "🌍",
             postText: "Want to go this beautifull place",
             postImage: "Waterfall",
             likePostNumber: "300",
             commentPostNumber: "50 Comments",
             sharePostNumber: "20 Share"),
    PostData(userPostProfileImage : "profile",
             userPostName: "Sida Hoil",
             postDate: "Just now",
             postLocation: "HRD",
             postStatus: "🌍",
             postText: "This is a third homework. It uses constraint to fixed each ui view in this app.This is a third homework. It uses constraint to fixed each ui view in this app.",
             postImage: "",
             likePostNumber: "1.5K",
             commentPostNumber: "100 Comments",
             sharePostNumber: "20 Shares"),
    PostData(userPostProfileImage : "profile",
             userPostName: "Flower Lover",
             postDate: "2 hrs",
             postLocation: "Phnom Penh",
             postStatus: "🌍",
             postText: "",
             postImage: "Waterfall",
             likePostNumber: "250",
             commentPostNumber: "35 Comments",
             sharePostNumber: "10 Shares"),
    PostData(userPostProfileImage : "profile",
             userPostName: "Sida Hoil",
             postDate: "10 hrs",
             postLocation: "Phnom Penh",
             postStatus: "🌍",
             postText: "Testing",
             postImage: "",
             likePostNumber: "300",
             commentPostNumber: "50 Comments",
             sharePostNumber: "20 Share")
]


