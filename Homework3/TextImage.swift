//
//  TextImage.swift
//  Homework3
//
//  Created by Apple on 3/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TextImage: UITableViewCell {

    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userPostName: UILabel!
    @IBOutlet weak var postText: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var postStatus: UIButton!
    @IBOutlet weak var postLocation: UILabel!
    @IBOutlet weak var likePostNumber: UIButton!
    @IBOutlet weak var commentPostNumber: UIButton!
    @IBOutlet weak var sharePostNumber: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileCircle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func profileCircle() {
        // make profile image circle
        userProfileImage.layer.cornerRadius = 25
        userProfileImage.layer.masksToBounds = true
    }
    
    
    //set new post information 
    func setPostInfo(postData : PostData) {
        self.userProfileImage.image = UIImage(named: postData.userPostProfileImage!)
        self.userPostName.text = postData.userPostName!
        self.postText.text = postData.postText!
        self.postImage.image = UIImage(named: postData.postImage!)
        self.postDate.text = postData.postDate!
        self.postStatus.setTitle(postData.postStatus!, for:  .normal)
        self.postLocation.text = postData.postLocation!
        self.likePostNumber.setTitle(postData.likePostNumber!, for: .normal)
        self.commentPostNumber.setTitle(postData.commentPostNumber!, for: .normal)
        self.sharePostNumber.setTitle(postData.sharePostNumber!, for: .normal)
    }
}
