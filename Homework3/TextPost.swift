//
//  TextPost.swift
//  Homework3
//
//  Created by Apple on 3/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TextPost: UITableViewCell {
    
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var postText: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userPostName: UILabel!
    @IBOutlet weak var postLocation: UILabel!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var postStatus: UIButton!
    @IBOutlet weak var textPost: UILabel!
    @IBOutlet weak var likePostNumber: UIButton!
    @IBOutlet weak var commentPostNumber: UIButton!
    @IBOutlet weak var sharePostNumber: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileCircle()
        
//        postText.adjustsFontSizeToFitWidth = true
        postText.font = UIFont.preferredFont(forTextStyle: .callout)
        postText.adjustsFontForContentSizeCategory = true
        
//        guard let customFont = UIFont(name: "CustomFont-Light", size: UIFont.labelFontSize) else {
//            fatalError("""
//        Failed to load the "CustomFont-Light" font.
//        Make sure the font file is included in the project and the font name is spelled correctly.
//        """
//            )
//        }
//        postText.font = UIFontMetrics.default.scaledFont(for: customFont)
//        postText.adjustsFontForContentSizeCategory = true
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    func profileCircle() {
        // make profile image circle
        profileImage.layer.cornerRadius = 25
        profileImage.layer.masksToBounds = true
    }
    
    
    //set new post information
    func setPostInfo(postData : PostData) {
        self.profileImage.image = UIImage(named: postData.userPostProfileImage!)
        self.userPostName.text = postData.userPostName!
        self.postDate.text = postData.postDate!
        self.postLocation.text = postData.postLocation!
        self.postStatus.setImage(UIImage(named: postData.postStatus!), for:.normal)
        self.textPost.text = postData.postText
        self.likePostNumber.setTitle(postData.likePostNumber!, for: .normal)
        self.commentPostNumber.setTitle(postData.commentPostNumber! , for:.normal)
        self.sharePostNumber.setTitle(postData.sharePostNumber!, for: .normal)
    }
    
}
